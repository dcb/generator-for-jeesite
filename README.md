#generator-for-jeesite 高扩展性的代码生成器，默认实现为jeesite代码生成器
##jeesite地址 http://git.oschina.net/thinkgem/jeesite.git
首先感谢ThinkGem君创作了jeesite，我从中受益匪浅，基于jeesite一直没有出好用的代码生成器，所以我写了一个，
如果ThinkGem君觉得还可以用，请直接带走，欢迎广大jeesite使用者拿去使用，如有问题请issue或联系我：QQ:601744346

#代码生成器使用方法：

一、快速使用：

1.下载zip包到本地，无需git

2.解压后找到release-1-0-0,启动run-generator.bat即可体验【output目录下会生成建表语句，entity，dao，service，controller类以及viewForm和viewList的jsp页面，对应拷贝即可】

3.修改Excel（新增Excel时注意修改config/config.properties中的excel文件名）配置你自己的表信息,生成器就可以为你打工了

4.Excel的内容，对应的是数据库的字段信息和增删改查页面信息（说明见excel/约定配置说明 及Excel注释）【建议拷贝sheet页然后修改，支持一次导出多个表的】

5.修改包名模块名功能名类作者信息等请修改config下的config.properties

二、下载源码编译后使用

1.git clone源码到本地

2.导入maven项目：eclipse——import--existing maven peoject--选择代码目录导入即可

3.run as --maven install 打包成功

4.到如下目录：XXX\generator-for-jeesite\jeesite-generator\realease 会看到maven生成的jar包和其他文件，执行run-generator.bat即可生成例子代码

5.使用同上3--5步骤

三、定制代码生成逻辑后使用

1.下载源码

2.修改源码或配置（其实你爱改啥爱改啥）

3.同上3--5步骤


#代码生成器简介
1.功能：通过Excel配置的字段信息生成基于jeesite的增删改查基本代码

2.支持freemarker和velocity两种模板引擎

3.Excel支持JXL和POI两种解析方式

4.目前仅支持mysql脚本的生成

5.提供的模板默认均为基于jeesite的代码生成

6.配置信息在config/config.properties配置

#代码生成器的扩展
如需扩展为其他框架生成代码，理论上只需要修改template下的模板和config.properties配置文件

#未来计划
1.支持更多数据库（oracle，sqlserver）的sql生成

2.如有需要，考虑支持更多框架的代码生成，这方面的需求请直接QQ我

#如果您有更多更好的建议
请QQ我：601744346