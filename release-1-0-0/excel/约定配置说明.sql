1、Excel按照模块划分，一个模块对应一个Excel文件
2、数据库表明明规则：最终表名=模块名_表名 （对应到Excel中就是sheet页的第一页名称+下划线+对应表的sheet页名称）
3、表和字段默认使用下划线划分多个单词，类名一律使用驼峰命名法
4、其他填写规则见Excel中的注释